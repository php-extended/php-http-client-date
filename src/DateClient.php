<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-date library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use DateTimeImmutable;
use DateTimeZone;
use InvalidArgumentException;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Stringable;

/**
 * DateClient class file.
 * 
 * This class is an implementation of a client which adds date headers of
 * incoming requests.
 * 
 * @author Anastaszor
 */
class DateClient implements ClientInterface, Stringable
{
	
	/**
	 * The inner client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * Builds a new DateClient with the given inner client.
	 * 
	 * @param ClientInterface $client
	 */
	public function __construct(ClientInterface $client)
	{
		$this->_client = $client;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		if(!$request->hasHeader('Date'))
		{
			try
			{
				$now = new DateTimeImmutable();
				$now = $now->setTimezone(new DateTimeZone('UTC'));
				$request = $request->withHeader('Date', $now->format('D, d M Y H:i:s \\G\\M\\T'));
			}
			// @codeCoverageIgnoreStart
			catch(InvalidArgumentException $e)
			// @codeCoverageIgnoreEnd
			{
				// nothing to do
			}
		}
		
		return $this->_client->sendRequest($request);
	}
	
}
