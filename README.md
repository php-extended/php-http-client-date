# php-extended/php-http-client-date
A psr-18 compliant middleware client that handles date headers.

![coverage](https://gitlab.com/php-extended/php-http-client-date/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-http-client-date/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-http-client-date ^8`


## Basic Usage

This library is to make a man in the middle for http requests and responses
and logs the events when requests passes. It may be used the following way :

```php

/* @var $client Psr\Http\Client\ClientInterface */    // psr-7
/* @var $request Psr\Http\Message\RequestInterface */ // psr-7

$client = new DateClient($client);
$response = $client->sendRequest($request);

/* @var $response Psr\Http\Message\ResponseInterface */

```

This library handles the adding of `Date` headers on the requests.


## License

MIT (See [license file](LICENSE)).
