<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-date library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\DateClient;
use PhpExtended\HttpMessage\Request;
use PhpExtended\HttpMessage\Response;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class DateClientClient implements ClientInterface
{
	
	public $request;
	
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		$this->request = $request;
		
		return new Response();
	}
	
}

/**
 * DateClientTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\DateClient
 *
 * @internal
 *
 * @small
 */
class DateClientTest extends TestCase
{
	
	/**
	 * The client to help.
	 * 
	 * @var DateClientClient
	 */
	protected DateClientClient $_client;
	
	/**
	 * The object to test.
	 * 
	 * @var DateClient
	 */
	protected DateClient $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testAddHeader() : void
	{
		$response = $this->_object->sendRequest(new Request());
		$this->assertInstanceOf(ResponseInterface::class, $response);
		$this->assertNotEmpty($this->_client->request->getHeaderLine('Date'));
	}
	
	public function testDoNothing() : void
	{
		$request = new Request();
		$request = $request->withHeader('Date', \date('Y-m-d'));
		$response = $this->_client->sendRequest($request);
		$this->assertInstanceOf(ResponseInterface::class, $response);
		$this->assertEquals(\date('Y-m-d'), $this->_client->request->getHeaderLine('Date'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_client = new DateClientClient();
		
		$this->_object = new DateClient($this->_client);
	}
	
}
